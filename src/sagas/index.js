import { takeLatest } from 'redux-saga/effects';
import { getItemsAsync } from 'components/feed/saga';
import { getItemByIdAsync } from 'components/item/saga';
import { GET_ITEMS, GET_ITEM_BY_ID } from 'constants/action-types';

export default function* rootSaga() {
  yield takeLatest(GET_ITEMS, getItemsAsync);
  yield takeLatest(GET_ITEM_BY_ID, getItemByIdAsync);
}

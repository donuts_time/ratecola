import { createSelector } from 'reselect';

const allItems = state => state.items;

export const getItems = createSelector(allItems, allItems => allItems);

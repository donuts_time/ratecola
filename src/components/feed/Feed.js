import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { getItems } from 'actions/index';
import { FeedItem } from 'components/feed/FeedItem';
import { Filter } from 'components/filter/Filter';
import { Loader } from 'components/common/Loader';
//import { getItems } from 'components/feed/selectors';

const mapStateToProps = state => ({
  items: state.items.allItems,
  loading: state.items.loading,
});

const mapDispatchToProps = dispatch => ({
  getItems: changedValue => dispatch(getItems(changedValue)),
});

const Aside = styled.aside`
  width: 240px;
  padding: 15px;
  margin-left: 40px;
  float: right;
  border: 1px solid #000;
`;

class ConnectedFeed extends Component {
  state = {
    filter: {
      manufacturer: '',
      origin: '',
    },
  };

  renderFeed() {
    if (this.props.items.length > 0) {
      return this.props.items.map(
        ({ id, name, description, manufacturer, img }) => (
          <FeedItem
            id={id}
            name={name}
            description={description}
            manufacturer={manufacturer}
            img={img}
            key={'feed-' + id}
            onTagClicked={this.filterChanged}
          />
        )
      );
    } else return <p>Nothing found</p>;
  }

  componentDidMount() {
    if (this.props.location.query) {
      const newFilter = { ...this.state.filter, ...this.props.location.query };
      this.setState({ filter: newFilter }, () => {
        this.props.getItems(this.state.filter);
      });
    } else {
      this.props.getItems(this.state.filter);
    }
  }

  filterChanged = changedValues => {
    const newFilter = { ...this.state.filter, ...changedValues };
    this.setState({ filter: newFilter }, () => {
      this.props.getItems(this.state.filter);
    });
  };

  render() {
    return (
      <div className="wrapper">
        <div className="feed">
          {this.props.loading ? <Loader /> : this.renderFeed()}
        </div>
        <Aside className="aside">
          <Filter
            filter={this.state.filter}
            filterChange={this.filterChanged}
            className="filter-wrapper"
          />
        </Aside>
      </div>
    );
  }
}

const Feed = connect(mapStateToProps, mapDispatchToProps)(ConnectedFeed);

export default Feed;

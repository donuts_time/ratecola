import { put, call } from 'redux-saga/effects';
import database from 'config/firebase';
import { getItems, getItemsSuccess, getItemsProgress } from 'actions/index';

export function* getItemsAsync({ payload: { filter } }) {
  yield put(getItemsProgress());

  const items = [];
  let dbQuery = database.collection('colas');

  if (filter) {
    const changedProperties = Object.entries(filter).filter(
      property => property[1] !== ''
    );
    dbQuery = changedProperties.reduce(
      (query, property) => query.where(property[0], '==', property[1]),
      dbQuery
    );
  }

  yield call(() =>
    dbQuery.get().then(querySnapshot => {
      querySnapshot.forEach(doc => {
        items.push({ id: doc.id, ...doc.data() });
      });
    })
  );
  yield put(getItemsSuccess(items));
}

export function* changeFilter({ payload: { filter } }) {
  yield put(changeFilter(filter));
  yield put(getItems());
}

import React from 'react';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';

const Article = styled.article`
  padding: 30px;
  margin-bottom: 30px;
  border-bottom: 1px solid #000;
  overflow: hidden;
  ${props =>
    props.primary &&
    css`
      background: #000;
    `};
`;

export const FeedItem = ({
  id,
  name,
  description,
  manufacturer,
  img = 'default.jpg',
  onTagClicked,
}) => {
  const handleTagClicked = (changedValue, e) => {
    e.preventDefault();
    onTagClicked(changedValue);
  };

  return (
    <Article className="feed-item">
      <Link className="feed-item-ico" to={'cola/' + id}>
        <img src={'img/' + img} alt={name} />
      </Link>
      <h1>
        <Link to={'cola/' + id}>{name}</Link>
      </h1>
      {manufacturer && (
        <p className="feed-item-manufacturer">
          By{' '}
          <a
            href="/"
            onClick={e => handleTagClicked({ manufacturer: manufacturer }, e)}
          >
            {manufacturer}
          </a>
        </p>
      )}
      <p className="feed-item-description">{description}</p>
    </Article>
  );
};

import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledHeader = styled.header`
  display: block;
  overflow: hidden;
`;

const Nav = styled.nav`
  margin-top: 25px;
  float: right;
  ul {
    list-style: none;
    li {
      display: inline-block;
      margin-left: 30px;
    }
  }
  a {
    color: #000;
    font-weight: 600;
    border-bottom: 1px solid transparent;
    transition: all 0.2s ease;
    &.active,
    &:hover {
      border-bottom-color: #000;
    }
  }
`;

const Logo = styled.a`
  position: relative;
  float: left;
  padding: 30px 0;
  font-weight: 600;
  font-size: 28px;
  text-decoration: none;
  color: #000;
  &::before,
  &::after {
      content: '';
      position: absolute;
      display: block;
      width: 100%;
      height: 4px;
      bottom: 25px;
      background: #000;
      transition: all 0.2s ease;
  }
  &::after {
      bottom: 15px;
      width: 100%;
  }
  &:hover {
    &::before,
    &::after {
      bottom: 20px;
      background: #E42A1D;
    }
`;

export const Header = () => {
  return (
    <StyledHeader>
      <div className="wrapper">
        <Logo href="/" className="logo">
          RateCola
        </Logo>
        <Nav>
          <ul>
            <li>
              <Link to="/" activeClassName="active">
                Home
              </Link>
            </li>
            <li>
              <Link to="/colas" activeClassName="active">
                All colas
              </Link>
            </li>
          </ul>
        </Nav>
      </div>
    </StyledHeader>
  );
};

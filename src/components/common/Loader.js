import React from 'react';
import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0%   { bottom: 10px; height: 20px; background: #000; }
  40%  { background: #000; }
  50%  { bottom: 0; height: 40px; background: #E42A1D; }
  600% { background: #000; }
  100% { bottom: 10px; height: 20px; background: #000; }
`;

const StyledLoader = styled.div`
  position: absolute;
  top: 100px;
  left: 50%;
  width: 100px;
  height: 50px;
  margin-left: -50px;
`;

const Line = styled.span`
  position: absolute;
  display: inline-block;
  bottom: 10px;
  left: 0;
  width: 6px;
  height: 20px;
  background: #000;
  margin-left: ${props => props.num * 11}px;
  animation: ${spin} 0.8s infinite both ease-out ${props => props.num * 0.1}s;
`;

export const Loader = () => (
  <StyledLoader>
    <Line num="0" />
    <Line num="1" />
    <Line num="2" />
    <Line num="3" />
  </StyledLoader>
);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { NavLink as Link } from 'react-router-dom';
import { getItemById } from 'actions/index';
import { Loader } from 'components/common/Loader';

const mapStateToProps = state => ({
  item: state.item.data,
  loading: state.item.loading,
});

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(getItemById(id)),
});

const ImgWrapper = styled.div`
  width: 320px;
  float: left;
  margin-right: 20px;
  img {
    max-width: 100%;
  }
`;

const Description = styled.div`
  width: 400px;
  float: left;
  .info {
    margin-top: 20px;
    font-size: 14px;
  }
`;

class ConnectedItemPage extends Component {
  renderItem() {
    const {
      name,
      description,
      manufacturer,
      origin,
      img = 'default.jpg',
    } = this.props.item;
    return (
      <div>
        <ImgWrapper>
          <img src={'img/' + img} alt={name} />
        </ImgWrapper>
        <Description>
          <h1>{name}</h1>
          <p>{description}</p>
          <div className="info">
            {manufacturer && (
              <p>
                Made By:{' '}
                <Link
                  to={{ pathname: '/', query: { manufacturer: manufacturer } }}
                >
                  {manufacturer}
                </Link>
              </p>
            )}
            {origin && <p>Country of origin: {origin}</p>}
          </div>
        </Description>
      </div>
    );
  }

  componentDidMount() {
    this.props.getItem(this.props.match.params.id);
  }

  render() {
    return (
      <div className="wrapper">
        {this.props.loading ? <Loader /> : this.renderItem()}
      </div>
    );
  }
}

const ItemPage = connect(mapStateToProps, mapDispatchToProps)(
  ConnectedItemPage
);

export default ItemPage;

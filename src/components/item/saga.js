import { put, call } from 'redux-saga/effects';
import database from 'config/firebase';
import { getItemByIdSuccess, getItemByIdProgress } from 'actions/index';

export function* getItemByIdAsync({ payload: { id } }) {
  yield put(getItemByIdProgress());

  const dbQuery = database.collection('colas').doc(id);

  const item = yield call(() =>
    dbQuery.get().then(querySnapshot => querySnapshot.data())
  );
  yield put(getItemByIdSuccess(item));
}

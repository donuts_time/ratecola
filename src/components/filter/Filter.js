import React from 'react';
import styled from 'styled-components';

const Select = styled.select`
  display: block;
  width: 100%;
  padding: 5px;
  background: #fff;
  margin-top: 20px;
  border: 1px solid #000;
  cursor: pointer;
  &:hover {
  border-color: red;
`;

export const Filter = ({
  filter = { origin: '', manufacturer: '' },
  filterChange,
}) => {
  const handleChange = e => {
    const changedProperty = {};
    changedProperty[e.target.name] = e.target.value;

    filterChange(changedProperty);
  };

  const handleSubmit = e => {
    e.preventDefault();
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>Search:</label>
      <Select
        name="manufacturer"
        value={filter.manufacturer}
        onChange={handleChange}
      >
        <option value="">Manufacturer</option>
        <option value="Fentimans">Fentimans</option>
        <option value="Lammsbrau">Lammsbrau</option>
      </Select>
      <Select name="origin" value={filter.origin} onChange={handleChange}>
        <option value="">Country of origin</option>
        <option value="Germany">Germany</option>
        <option value="UK">UK</option>
      </Select>
    </form>
  );
};

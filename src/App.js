import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Feed from './components/feed/Feed.js';
import { Header } from './components/common/Header';
import ItemPage from './components/item/ItemPage';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <main>
          <Header />
          <section className="content">
            <Switch>
              <Route exact path="/" component={Feed} />
              <Route exact path="/colas" component={Feed} />
              <Route path="/cola/:id" component={ItemPage} />
            </Switch>
          </section>
        </main>
      </BrowserRouter>
    );
  }
}

export default App;

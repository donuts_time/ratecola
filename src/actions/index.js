import {
  GET_ITEMS,
  GET_ITEMS_SUCCEEDED,
  GET_ITEMS_PROGRESS,
  GET_ITEM_BY_ID,
  GET_ITEM_BY_ID_SUCCEEDED,
  GET_ITEM_BY_ID_PROGRESS,
} from 'constants/action-types';

export const getItemsSuccess = data => {
  return {
    type: GET_ITEMS_SUCCEEDED,
    payload: {
      items: data,
    },
  };
};

export const getItemsProgress = filter => {
  return {
    type: GET_ITEMS_PROGRESS,
    payload: {
      filter: filter,
    },
  };
};

export const getItems = filter => {
  return {
    type: GET_ITEMS,
    payload: {
      filter: filter,
    },
  };
};

export const getItemById = id => {
  return {
    type: GET_ITEM_BY_ID,
    payload: {
      id: id,
    },
  };
};

export const getItemByIdSuccess = data => {
  return {
    type: GET_ITEM_BY_ID_SUCCEEDED,
    payload: {
      item: data,
    },
  };
};

export const getItemByIdProgress = () => {
  return {
    type: GET_ITEM_BY_ID_PROGRESS,
  };
};

import {
  GET_ITEMS_PROGRESS,
  GET_ITEMS_SUCCEEDED,
} from 'constants/action-types';

const initialState = { loading: false, allItems: [], filter: [] };

const itemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEMS_PROGRESS:
      return { ...state, loading: true };
    case GET_ITEMS_SUCCEEDED:
      return { ...state, loading: false, allItems: action.payload.items };
    default:
      return state;
  }
};

export default itemsReducer;

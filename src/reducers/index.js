import { combineReducers } from 'redux';
import itemsListReducer from 'reducers/itemsListReducer';
import itemReducer from 'reducers/itemReducer';

export default combineReducers({
  items: itemsListReducer,
  item: itemReducer,
});

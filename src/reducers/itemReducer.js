import {
  GET_ITEM_BY_ID_PROGRESS,
  GET_ITEM_BY_ID_SUCCEEDED,
} from 'constants/action-types';

const initialState = { loading: false, data: {} };

const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEM_BY_ID_PROGRESS:
      return { ...state, loading: true };
    case GET_ITEM_BY_ID_SUCCEEDED:
      return { loading: false, data: action.payload.item };
    default:
      return state;
  }
};

export default itemReducer;

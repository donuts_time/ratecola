import * as firebase from 'firebase';

import { FirebaseConfig } from '../config/keys';
firebase.initializeApp(FirebaseConfig);

const database = firebase.firestore();

database.settings({ timestampsInSnapshots: true });

export default database;
